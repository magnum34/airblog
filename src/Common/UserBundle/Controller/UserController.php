<?php

namespace Common\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use \Symfony\Component\HttpFoundation\Request;
use Common\UserBundle\Form\Type\AccountSettingsType;
use Common\UserBundle\Form\Type\ChangePasswordType;
use \Common\UserBundle\Exception\UserException;

class UserController extends Controller {
    /**
     * @Route("/account-settings",name="user_accountSettings")
     */
    public function accountSettingsAction(Request $request){
        //Account Settings Form
        $user = $this->getUser();
        $accountSettingsForm = $this->createForm(new AccountSettingsType(),$user);
        if($request->isMethod('POST') && $request->request->has('accountSettings')){
            $accountSettingsForm->handleRequest($request);
            if($accountSettingsForm->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
                
                $this->get('session')->getFlashBag()->add('success','Twoje Dane zostały zmienione');
                return $this->redirect($this->generateUrl('user_accountSettings'));
            }else{
                 $this->get('session')->getFlashBag()->add('error','Popraw błedy formularzu');

            }
        }
        
        //Change Password
        $changePasswordForm = $this->createForm(new ChangePasswordType(), $user);
        if($request->isMethod('POST') && $request->request->has('changePassword')){
            $changePasswordForm->handleRequest($request);
            if($changePasswordForm->isValid()){
                try{
                    $userManager = $this->get('user_manager');
                    $userManager->changePassword($user);
                    $this->get('session')->getFlashBag()->add('success','Twoje hasło zostało zmienione');
                return $this->redirect($this->generateUrl('user_accountSettings'));
                    
                    
                } catch (UserException $ex) {
                    $this->get('session')->getFlashBag()->add('error',$ex->getMessage()); 
                }
            }else{
               $this->get('session')->getFlashBag()->add('error','Popraw błedy formularzu'); 
            }
        }
        
        return $this->render('CommonUserBundle:User:accountSettings.html.twig',array(
            'user' => $user,
            'accountSettingsForm' => $accountSettingsForm->createView(),
            'changePassword' => $changePasswordForm->createView()
        ));
    }
}
