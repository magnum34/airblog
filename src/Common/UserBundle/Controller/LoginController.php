<?php

namespace Common\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\Request;
use Common\UserBundle\Form\Type\LoginType;
use Common\UserBundle\Form\Type\RememberPasswordType;
use Symfony\Component\Form\FormError;
use Common\UserBundle\Exception\UserException;
use \Common\UserBundle\Entity\User;
use \Common\UserBundle\Form\Type\RegisterUserType;

class LoginController extends Controller
{
    /**
     * 
     * @Route("/login",name="blog_login")
     */
    public function loginAction(Request $request)
    {
        $session = $this->get('session');
        
        
        if($request->attributes->has(SecurityContextInterface::AUTHENTICATION_ERROR)){
            $loginError = $request->attributes->get(SecurityContextInterface::AUTHENTICATION_ERROR);
        }else{
            $loginError = $session->remove(SecurityContextInterface::AUTHENTICATION_ERROR);
        }
        if(isset($loginError)){
            $this->get('session')->getFlashBag()->add('error',$loginError->getMessage());

        }
        
        $loginForm = $this->createForm(new LoginType(),array(
           'username' =>  $session->get(SecurityContextInterface::LAST_USERNAME)
        ));
        $rememberPasswordForm = $this->createForm(new RememberPasswordType());
        
        if($request->isMethod('POST')){
            $rememberPasswordForm->handleRequest($request);
            
            if($rememberPasswordForm->isValid()){
                try{
                    $userEmail = $rememberPasswordForm->get('email')->getData();
                    $userManager = $this->get('user_manager');
                    $userManager->sendResetPasswordLink($userEmail);
                    $this->get('session')->getFlashBag()->add('success','Instrukcja resetująca hasła została wysłana na adres e-mail');
                    
                    return $this->redirect($this->generateUrl('blog_login'));
                            
                            
                } catch (UserException $ex) {
                    $error = new FormError($ex->getMessage());
                    $rememberPasswordForm->get('email')->addError($error);
                }
                
            }
            
        }
        
        //register user
        $user = new User();
        $registerUserForm = $this->createForm(new RegisterUserType(),$user);
        if($request->isMethod('POST')){
            $registerUserForm->handleRequest($request);
            if($registerUserForm->isValid()){
                try{
                    $userManager = $this->get('user_manager');
                    $userManager->registerUser($user);
                    $this->get('session')->getFlashBag()->add('success','Konto zostalo ułożone. Na Twoją skrzynkę pocztową została wysłana wiadomość aktywacyjna.');
                    return $this->redirect($this->generateUrl('blog_login'));
                } catch (UserException $ex) {
                   $this->get('session')->getFlashBag()->add('error',$loginError->getMessage()); 
                }
            }
        }
        return $this->render('CommonUserBundle:Login:login.html.twig',array(
            'loginForm' => $loginForm->createView(),
            'rememberPasswordForm' => $rememberPasswordForm->createView(),
            'registerUserForm' => $registerUserForm->createView()
        ));
    }
    /**
     * 
     * @Route("/reset-password/{actionToken}",name="user_resetPassword")
     */
    public function resetPasswordAction($actionToken){
        try{
            $userManager = $this->get('user_manager');
            $userManager->resetPassword($actionToken);
            
            $this->get('session')->getFlashBag()->add('success','Na twój adres e-mail zostało wysłane nowe hasło');
        } catch (UserException $ex) {
            $this->get('session')->getFlashBag()->add('error',$ex->getMessage());

        }
        return $this->redirect($this->generateUrl('blog_login'));
    }
    /**
     * 
     * @Route("/account-activation/{actionToken}",name="user_activationAccount")
     */
    public function activateAccountAction($actionToken){
        try{
            $userManager = $this->get('user_manager');
            $userManager->activateAccount($actionToken);
            
            $this->get('session')->getFlashBag()->add('success','Twoje konto zostało aktywne');
        } catch (UserException $ex) {
            $this->get('session')->getFlashBag()->add('error',$ex->getMessage());

        }
        return $this->redirect($this->generateUrl('blog_login'));
    }
}
