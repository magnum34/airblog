<?php

namespace Common\UserBundle\Manager;

use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface as Templating;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Common\UserBundle\Mailer\UserMailer;
use Common\UserBundle\Exception\UserException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Common\UserBundle\Entity\User;


class UserManager {
    protected $doctrine;
    protected $router;
    protected $templating;
    protected $encoderFactory;
    protected $userMailer;
    public function __construct(Doctrine $doctrine,Router $router,Templating $templating,
     EncoderFactory $encoderFactory,UserMailer $userMailer) {
        $this->doctrine = $doctrine;
        $this->router = $router;
        $this->templating = $templating;
        $this->encoderFactory = $encoderFactory;
        $this->userMailer = $userMailer;
    }
    protected function generateActionToken(){
        return substr(md5(uniqid(NULL,TRUE)),0,20);
    }
    protected function getRandomPassword($length = 8){
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
    public function sendResetPasswordLink($userMail){
        $user = $this->doctrine->getRepository('CommonUserBundle:User')
                ->findOneBy(array(
                    'mail' => $userMail
                ));
        if(null === $user){
            throw new UserException('Nie znaleziono takiego użytkownika');
        }
        $user->setActionToken($this->generateActionToken());
        
        $em = $this->doctrine->getManager();
        $em->persist($user);
        $em->flush();
        $urlParams = array('actionToken' => $user->getActionToken());
        $resetUrl = $this->router->generate('user_resetPassword',$urlParams,  UrlGeneratorInterface::ABSOLUTE_URL);
        
        $emailBody = $this->templating->render('CommonUserBundle:Email:passwdResetLink.html.twig',
                array('resetUrl' => $resetUrl));
        $this->userMailer->send($user, 'Email resetujący hasło', $emailBody);
        
        return true;
    }
    public function resetPassword($actionToken){
        $user = $this->doctrine->getRepository('CommonUserBundle:User')
                ->findOneBy(array(
                    'actionToken' => $actionToken
                ));
        if(null === $user){
            throw new UserException('Podane błedne parametry akcji.');
        }
        $plainPassword = $this->getRandomPassword();
        
        $encoder = $this->encoderFactory->getEncoder($user);
        
        $encoderPassword = $encoder->encodePassword($plainPassword, $user->getSalt());
        
        $user->setPassword($encoderPassword);
        $user->setActionToken(null);
        
        $em = $this->doctrine->getManager();
        $em->persist($user);
        $em->flush();
        
        $emailBody = $this->templating->render('CommonUserBundle:Email:newPassword.html.twig',
                array('plainPassword' => $plainPassword));
        $this->userMailer->send($user, 'Nowe hasło to konta', $emailBody);
        
        return true;
    }
    public function registerUser(User $user){
        if(null !== $user->getId()){
            throw new UserException('Użytkownik jest już zarejestrowany');
        }
        $encoder = $this->encoderFactory->getEncoder($user);
        $encodedPasswd = $encoder->encodePassword($user->getPlainPassword(), $user->getSalt());
        
        $user->setPassword($encodedPasswd);
        $user->setActionToken($this->generateActionToken());
        $user->setEnabled(false);
        
        $em = $this->doctrine->getManager();
        $em->persist($user);
        $em->flush();
        
        $urlParams = array('actionToken' => $user->getActionToken());
        $activationUrl = $this->router->generate('user_activationAccount',$urlParams,  UrlGeneratorInterface::ABSOLUTE_URL);
        
        $emailBody = $this->templating->render('CommonUserBundle:Email:accountActivation.html.twig',
                array('activationUrl' => $activationUrl));
        $this->userMailer->send($user, 'Aktywacja konta ', $emailBody);
        return true;
    }
    public function activateAccount($actionToken){
        $user = $this->doctrine->getRepository('CommonUserBundle:User')
                ->findOneBy(array(
                    'actionToken' => $actionToken
                ));
        if(null === $user){
            throw new UserException('Podane błedne parametry akcji.');
        }
        
        $user->setEnabled(true);
        $user->setActionToken(null);
        
        $em = $this->doctrine->getManager();
        $em->persist($user);
        $em->flush();
        
        return true;
    }
    public function changePassword(User $user){
        if(null === $user->getPlainPassword()){
            throw new UserException('Nie ustawiono nowego hasła');
        }
        
        $encoder = $this->encoderFactory->getEncoder($user);
        $encoderPassword = $encoder->encodePassword($user->getPlainPassword(), $user->getSalt());
        $user->setPassword($encoderPassword);
        
        $em = $this->doctrine->getManager();
        $em->persist($user);
        $em->flush();
        
        return TRUE;
    }
    
}
