<?php
namespace Common\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type;

class AccountSettingsType extends AbstractType{
    public function getName() {
        return 'accountSettings';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('username',  Type\TextType::class,array(
            'label' => 'Nick',
            'required' => FALSE
        ))->add('avatarFile',  Type\FileType::class,array(
            'label' => 'Zmień avatar',
            'required' => FALSE
        ))->add('submit',  Type\SubmitType::class,array(
            'label' => 'Zapisz zmiany'
        ));
    }
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
       $resolver->setDefaults(array(
                'data_class' => 'Common\UserBundle\Entity\User',
                'validation_groups' => array('Default', 'ChangeDetails')
        ));
    }
    
}
