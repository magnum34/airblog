<?php

namespace Common\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Validator\Constraints as Assert;


class RememberPasswordType extends AbstractType {
    public function getName() {
        return 'rememberPassword';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('email',  Type\EmailType::class,array(
            'label' => 'Twój e-mail',
            'constraints' => array(
                new Assert\NotBlank(),
                new Assert\Email()
            )
        ))->add('submit',  Type\SubmitType::class,array(
            'label' => 'Przypomnij hasło'
        ));
    }
}
