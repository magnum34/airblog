<?php
namespace Common\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;

class LoginType extends AbstractType {
    public function getName(){
        return 'login';
    }
    public function buildForm(FormBuilderInterface $builder,array $oprions){
        $builder->add('username', Type\TextType::class,array(
            'label' => 'Login'
        ))->add('password',  Type\PasswordType::class,array(
            'label' => 'Hasło'
        ))->add('remeber_me',  Type\CheckboxType::class,array(
            'label' => 'Zapamiętaj mnie'
        ))->add('save',  Type\SubmitType::class,array(
            'label' => 'Zaloguj'
        ));
    }
}

?>

