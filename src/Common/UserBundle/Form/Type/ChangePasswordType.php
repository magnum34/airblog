<?php

namespace Common\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

class ChangePasswordType extends AbstractType{
    public function getName(){
        return 'changePassword';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('currentPassword',  Type\PasswordType::class,array(
            'label' => 'Aktualne hasło',
            'mapped' => false,
            'constraints' => array(
                new UserPassword(array(
                    'message' => 'Podano błędne aktualne hasło użytkownika'
                ))
            )
        ))->add('plainPassword',  Type\RepeatedType::class,array(
            'type' => Type\PasswordType::class,
            'first_options' => array(
                'label' => 'Nowe Hasło'
            ),
            'second_options' => array(
                'label' => 'Powtórz Hasło'
            )
            
        ))->add('submit', Type\SubmitType::class,array(
            'label' => 'Zmień Hasło'
        ));
    }
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
       $resolver->setDefaults(array(
                'data_class' => 'Common\UserBundle\Entity\User',
                'validation_groups' => array('Default', 'ChangePassword')
        ));
    }
    
}
