<?php

namespace Common\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RegisterUserType extends AbstractType {
    public function getName() {
        return 'userRegister';
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('mail',  Type\EmailType::class,array(
            'label'=> 'E-mail'
        ))->add('username',  Type\TextType::class,array(
            'label' => 'Nick'
        ))->add('plainPassword',  Type\RepeatedType::class,array(
           'type' => Type\PasswordType::class,
            'first_options' => array(
                'label' => 'Hasło'
            ),
            'second_options' => array(
                'label' => 'Powtórz hasło'
            )
        ))->add('submit', Type\SubmitType::class,array(
            'label' => 'Zarejestruj'
        ));
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
                'data_class' => 'Common\UserBundle\Entity\User',
                'validation_groups' => array('Default', 'Registration')
        ));
    }
}
