<?php
namespace Common\UserBundle\Mailer;

use Common\UserBundle\Entity\User;

class UserMailer {
    private $swiftMailer;
    private $fromEmail;
    private $fromName;
    
    public function __construct(\Swift_Mailer $swiftMailer,$fromEmail,$fromName) {
        $this->swiftMailer = $swiftMailer;
        $this->fromName = $fromName;
        $this->fromEmail = $fromEmail;
    }
    public function send(User $user,$subject,$htmlBody){
        $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($this->fromEmail, $this->fromName)
                ->setTo($user->getMail(), $user->getUsername())
                ->setBody($htmlBody,'text/html');
        $this->swiftMailer->send($message);
    }
}
