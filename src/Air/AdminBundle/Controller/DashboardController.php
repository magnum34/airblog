<?php

namespace Air\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DashboardController extends Controller {
    /**
     * @Route("/",name="admin_dashboard")
     */
    public function indexAction(){
        return $this->render('AirAdminBundle:Dashboard:index.html.twig', array());
    }
}
