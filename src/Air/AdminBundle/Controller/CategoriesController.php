<?php

namespace Air\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CategoriesController extends Controller {
    public function indexAction(){
        return $this->render('AirAdminBundle:Categories:index.html.twig', array());
    }
}
