<?php

namespace Air\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;

class PostsController extends Controller{
    /**
     *@Route("/list/{status}/{page}",
     * name="admin_postList",
     * requirements={"page"="\d+"},
     * defaults={"status"="all","page"=1})
     */
    public function indexAction(Request $request,$status,$page){
        $queryParams = array(
            'titleLike' => $request->query->get('titleLike'),
            'categoryId' => $request->query->get('categoryId'),
            'status' => $status
        );
        $postRepository = $this->getDoctrine()->getRepository('AirBlogBundle:Post');
        $qb = $postRepository->getQueryBuilder($queryParams);
        
        $paginationLimit = $this->container->getParameter('admin.pagination_limit');
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($qb,$page,$paginationLimit);
        
        return $this->render('AirAdminBundle:Posts:index.html.twig', array(
            'currPage' => 'posts',
            'pagination' => $pagination
        ));
    }
}
