<?php

namespace Air\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TagsController extends Controller{
    public function indexAction(){
        return $this->render('AirAdminBundle:Tags:index.html.twig', array());
    }
}
