<?php
namespace Air\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PagesController extends Controller{
    /**
     * @Route("/about",name="blog_about")
     */
    public function aboutAction(){
        return $this->render('AirBlogBundle:Pages:about.html.twig');
    }
    /**
     * @Route("/contact",name="blog_contact")
     */
    public function contactAction(){
        return $this->render('AirBlogBundle:Pages:contact.html.twig');
    }
}
