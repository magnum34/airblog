<?php

namespace Air\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Air\BlogBundle\Entity\Comment;
use Air\BlogBundle\Form\Type\CommentType;


class PostsController extends Controller
{
    protected $itemsLimit = 3;
    /**
     * @Route("/{page}",
     * name="blog_index",
     * defaults={"page" = 1},
     * requirements={"page"="\d+"}
     * )
     */
    public function indexAction($page)
    {
        
        //$allPosts = $PostRepo->findBy(array(), array('publishedDate'=>'desc'));
        $pagination = $this->getPaginatedPosts(array(
            'status'=>'published',
            'orderBy'=>'p.publishedDate',
            'orderDir'=>'DESC'
        ),$page);
        
        return $this->render('AirBlogBundle:Posts:postsList.html.twig',array(
            'pagination' => $pagination,
            'ListTitle' => 'Najnowsze wpisy'
        ));
    }
    /**
     * @Route("/search/{page}",
     * name="blog_search",
     * defaults={"page" = 1},
     * requirements={"page"="\d+"}
     * )
     */
    public function searchAction(\Symfony\Component\HttpFoundation\Request $request,$page)
    {
        
        $searchParam = $request->query->get('search');
 
        $pagination = $this->getPaginatedPosts(array(
            'status'=>'published',
            'orderBy'=>'p.publishedDate',
            'orderDir'=>'DESC',
            'search'=>$searchParam
        ),$page);
        
        return $this->render('AirBlogBundle:Posts:postsList.html.twig',array(
            'pagination' => $pagination,
            'ListTitle' => sprintf('Wyniki wyszukiwania "%s"',$searchParam),
            'searchParam' => $searchParam
        ));
    }
    /**
     * @Route("/{slug}",name="blog_post")
     */
    public function postAction(\Symfony\Component\HttpFoundation\Request $request,$slug){
        $postRepo = $this->getDoctrine()->getRepository('AirBlogBundle:Post');
        $post = $postRepo->getPublishedPost($slug);
        if(null == $post){
            throw $this->createNotFoundException('Post nie został odnaleziony');
        }
        
        if(null !== $this->getUser()){
            
            $comment = new Comment();
            $comment->setAuthor($this->getUser())
                    ->setPost($post);
            $commentForm = $this->createForm(new CommentType(),$comment);
            if($request->getMethod() == 'POST'){
                $commentForm->handleRequest($request);
                if($commentForm->isValid()){
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($comment);
                    $em->flush();
                    
                    
                    $this->get('session')->getFlashBag()->add('success','Komentarz Dodany');
                    $redirectUrl = $this->generateUrl('blog_post', array(
                        'slug' => $post->getSlug()
                    ));
                    
                    return $this->redirect($redirectUrl);
                }
                
            }
        }
        if($this->get('security.context')->isGranted('ROLE_ADMIN')){
            $csrfProvider = $this->get('form.csrf_provider');
            
        }
        
        
        return $this->render('AirBlogBundle:Posts:post.html.twig',array(
            'post'=>$post,
            'commentForm' => isset($commentForm) ? $commentForm->createView() : null,
            'csrfProvider' => isset($csrfProvider) ? $csrfProvider : null,
            'tokenName' => 'delCom%d'
        ));
    }
    /**
     * 
     *@Route("/post/comment/delete/{commentId}/{token}",name="blog_deleteComment")
     */
    
    public function deleteCommentAction($commentId,$token){
        
        if(!$this->get('security.context')->isGranted('ROLE_ADMIN')){
            throw $this->createAccessDeniedException('Nie masz uprawnień do tego zadania');
            
        }
        $validToken = sprintf('delCom%d', $commentId);
        if(!$this->get('form.csrf_provider')->isCsrfTokenValid($validToken, $token)){
            throw $this->createAccessDeniedException('Błędny token akcji');
        }
        
        $comment = $this->getDoctrine()->getRepository('AirBlogBundle:Comment')
                ->find($commentId);
        if(null == $comment){
            throw $this->createNotFoundException('Nie znaleziono takiego komentarza');
        }
        
        $em = $this->getDoctrine()->getManager();
        $em->remove($comment);
        $em->flush();
        
        
        return new \Symfony\Component\HttpFoundation\JsonResponse(array(
            'status' => 'ok',
            'message' => 'Wiadomość została usunieta'
        ));
    }
    /**
     * @Route("/category/{slug}/{page}",
     * name="blog_category",
     * defaults={"page" = 1},
     * requirements={"page"="\d+"}
     * )
     */
    public function categoryAction($slug,$page){
        
        $categoryRepo = $this->getDoctrine()->getRepository('AirBlogBundle:Category');
        $category = $categoryRepo->findOneBy(array('slug'=>$slug));
        
        $pagination = $this->getPaginatedPosts(array(
            'status'=>'published',
            'orderBy'=>'p.publishedDate',
            'orderDir'=>'DESC',
            'categorySlug'=>$slug
        ),$page);
        
        return $this->render('AirBlogBundle:Posts:postsList.html.twig',array(
            'pagination' => $pagination,
            'ListTitle' => sprintf('Wpisy w katgerii "%s"',$category->getName())
        ));
    }
    /**
     * @Route("/tag/{slug}/{page}",
     * name="blog_tag",
     * defaults={"page" = 1},
     * requirements={"page"="\d+"}
     * )
     */
    public function tagAction($slug,$page){
        
        $TagRepo = $this->getDoctrine()->getRepository('AirBlogBundle:Tag');
        $Tag = $TagRepo->findOneBy(array('slug'=>$slug));
        
        $pagination = $this->getPaginatedPosts(array(
            'status'=>'published',
            'orderBy'=>'p.publishedDate',
            'orderDir'=>'DESC',
            'tagSlug'=>$slug
        ),$page);
        
        return $this->render('AirBlogBundle:Posts:postsList.html.twig',array(
            'pagination' => $pagination,
            'ListTitle' => sprintf('Wpisy z tagiem "%s"',$Tag->getName())
        ));
    }
    protected function getPaginatedPosts(array $params = array(),$page){
        $PostRepo = $this->getDoctrine()->getRepository('AirBlogBundle:Post');

        $qb = $PostRepo->getQueryBuilder($params);
        
        $paginator = $this->get('knp_paginator');       
        $pagination = $paginator->paginate($qb,$page,$this->itemsLimit);
        
        return $pagination;
    }
}
