<?php

namespace Air\BlogBundle\Repository;

use Doctrine\ORM\EntityRepository;

class TagRepository  extends EntityRepository{
    
    public function getTagsListOcc(){
        $qb = $this->createQueryBuilder('t')
                ->select('t.name, t.slug, COUNT(p) as occ')
                ->leftJoin('t.posts', 'p')
                 ->groupBy('t.name');
        return $qb->getQuery()->getArrayResult();
    }
}
