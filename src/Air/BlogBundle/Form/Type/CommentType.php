<?php

namespace Air\BlogBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CommentType extends AbstractType {
    public function getName(){
        return 'comment';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('comment',  Type\TextareaType::class,array(
            'label' => 'komentarz'
        ))->add('save',  Type\SubmitType::class,array(
           'label' => 'Dodaj' 
        ));
    }
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setOptional(array(
                'data_class' => 'Air\BlogBundle\Entity\Comment'
        ));
    }
}
