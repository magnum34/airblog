<?php

namespace Air\BlogBundle\Twig\Extension;

class BlogExtension  extends \Twig_Extension{
    /**
     *
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    private $doctrine;
    /**
     *
     * @var \Twig_Enironment
     */
    private $environment;
    
    private $categoryList;
    
    function __construct(\Doctrine\Bundle\DoctrineBundle\Registry $doctrine) {
        $this->doctrine = $doctrine;
    }
    public function initRuntime(\Twig_Environment $environment) {
        $this->environment = $environment;
    }
    public function getName() {
        return 'air_blog';
    }
    public function getFunctions() {
        return array(
            new \Twig_SimpleFunction('print_category_list', array($this,'printCategoryList'),
                    array('is_safe'=>array('html'))),
            new \Twig_SimpleFunction('print_main_menu', array($this,'printMainMenu'),
                    array('is_safe'=>array('html'))),
            new \Twig_SimpleFunction('print_tags_cloud', array($this,'tagsCloud'),
                    array('is_safe'=>array('html'))),
            new \Twig_SimpleFunction('print_recent_commented',array($this,'recentCommented'),
            array('is_safe'=>array('html')))
            
        );
    }
    public function getFilters() {
        return array(
          new \Twig_SimpleFilter('ab_shorten',array($this,'shorten'),array('is_safe'=>array('html')))  
        );
    }

    public function printCategoryList(){
        if(!isset($this->categoryList)){
            $categoryRepo = $this->doctrine->getRepository('AirBlogBundle:Category');
            $this->categoryList = $categoryRepo->findAll();
        }
        
        return $this->environment->render('AirBlogBundle:Template:categoryList.html.twig',array(
            'categoryList' => $this->categoryList
        ));
    }
    public function printMainMenu(){
        $mainMenu = array(
          'home' => 'blog_index',
            'o mnie' => 'blog_about',
            'kontakt' => 'blog_contact'
        );
        return $this->environment->render('AirBlogBundle:Template:mainMenu.html.twig',array(
            'mainMenu' => $mainMenu
        ));
    }
    public function tagsCloud($limit = 40,$minFontSize = 1, $maxFontSize = 3.5){
        $TagRepo = $this->doctrine->getRepository('AirBlogBundle:Tag');
        $tagsList = $TagRepo->getTagsListOcc();
        $tagsList = $this->prepareTagsCould($tagsList, $limit, $minFontSize, $maxFontSize);
        
        return $this->environment->render('AirBlogBundle:Template:tagsCloud.html.twig',array(
            'tagsList' => $tagsList
        ));
    }
    public function shorten($text,$length = 200,$wrapTag = 'p'){
        //ignorowanie znaków , znaczników html5
        $text = strip_tags($text);
        $text = substr($text,0,$length).'[...]';
        
        $openTag = "<{$wrapTag}>";
        $closeTag = "</{$wrapTag}";
        return $openTag.$text.$closeTag;
        
        
    }
    protected function prepareTagsCould($tagsList, $limit,$minFontSize, $maxFontSize ){
        $occs = array_map(function($row){
            return (int)$row['occ'];
        }, $tagsList);
        
        $minOcc = min($occs);
        $maxOcc = max($occs);
        
        $spread = $maxOcc - $minOcc;
        
        $spread = ($spread == 0) ? 1 : $spread;
        
        usort($tagsList, function($a, $b){
            $ao = $a['occ'];
            $bo = $b['occ'];
            
            if($ao === $bo) return 0;
            
            return ($ao < $bo) ? 1 : -1;
        });
        
        $tagsList = array_slice($tagsList, 0, $limit);
        
        shuffle($tagsList);
        
        foreach($tagsList as &$row){
            $row['fontSize'] = round(($minFontSize + ($row['occ'] - $minOcc) * ($maxFontSize - $minFontSize) / $spread), 2);
        }

        return $tagsList;
    }
    public function  recentCommented($limit = 3){
        $PostRepo = $this->doctrine->getRepository('AirBlogBundle:Post');
        
        $postList = $PostRepo->getRecentCommand($limit);
        
        return $this->environment->render('AirBlogBundle:Template:recentCommend.html.twig',array(
            'postsList' => $postList
        ));
    }

}
