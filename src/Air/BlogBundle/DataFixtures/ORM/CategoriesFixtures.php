<?php

namespace Air\BlogBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Air\BlogBundle\Entity\Category;

class CategoriesFixtures extends AbstractFixture implements OrderedFixtureInterface{
    
    public function load(ObjectManager $manager){
        $categoryList = array(
          'osobowe' => 'Samoloty osobowe i pasażerskie',
            'odrzutowe' => 'Samoloty odrzutowe',
            'wojskowe' => 'Samoloty wojskowe',
            'kosmiczne' => 'Promy kosmiczne',
            'tajne' => 'Tajne rozwiązania'
        );
        foreach ($categoryList as $key => $name) {
            $category = new Category();
            $category->setName($name);
            $manager->persist($category);
            $this->addReference('category_'.$key, $category);
        }
        
        

        
        $manager->flush();
    }

    public function getOrder() {
        return 0;
    }

}
